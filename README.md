Requirements:  

- terraform version 0.12 or higher.  
```
terraform [ init | validate | plan | apply | destroy ]
```

- credentials file located at ~/.aws/credentials
```
[terraform]
aws_access_key_id = XXX
aws_secret_access_key = XXX

```

