# AWS AMI		https://www.terraform.io/docs/providers/aws/d/ami.html

# Get the latest ami id
data "aws_ami" "AmazonLinux2_latest" {
most_recent = true
owners = ["591542846629"] # AWS
  filter {
      name              = "name"
      values            = ["*amazon-ecs-optimized"]
  }
  filter {
      name              = "virtualization-type"
      values            = ["hvm"]
  }
}

