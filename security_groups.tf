# AWS security_groups		https://www.terraform.io/docs/providers/aws/r/security_group.html

resource "aws_security_group" "inter_vpc" {
  name		= "inter_vpc"
  vpc_id	= aws_vpc.vpc.id
  tags = {
    Name	= "inter_vpc"
  }

  ingress {
    self	= true
    from_port	= 0
    to_port	= 0
    protocol	= "-1"
  }

  egress {
    from_port	= 0
    to_port	= 0
    protocol	= "-1"
    cidr_blocks	= ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ssh" {
  name		= "ssh"
  vpc_id	= aws_vpc.vpc.id
  tags = {
    Name	= "ssh"
  }
  ingress {
    from_port	= 22
    to_port	= 22
    protocol	= "tcp"
    cidr_blocks	= ["0.0.0.0/0"]
  }
  egress {
    from_port	= 0
    to_port	= 0
    protocol	= "-1"
    cidr_blocks	= ["0.0.0.0/0"]
  }
}
