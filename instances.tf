# AWS instances			https://www.terraform.io/docs/providers/aws/r/instance.html
resource "aws_instance" "node1" {
  #ami				= "${var.ami["AmazonLinux2"]}"
  ami				= data.aws_ami.AmazonLinux2_latest.id	# defined under ami.tf
  availability_zone		= var.az["subnet_a"] 
  instance_type			= "t2.micro"
  subnet_id                     = aws_subnet.subnet_a.id
  private_ip                    = "10.0.1.5"  
  associate_public_ip_address	= "true"
  key_name			= var.ssh_keys["admins"]
  vpc_security_group_ids	= ["${aws_security_group.inter_vpc.id}", "${aws_security_group.ssh.id}"]
  source_dest_check		= "false"

  tags = {
	Name			= "node1"
	Environment		= "test"
  }

  #lifecycle {
  #  ignore_changes		= ["private_ip", "root_block_device", "ebs_block_device"]
  #}

  user_data = <<EOF
  #!/bin/bash
  sudo yum update -y
  sudo yum install -y python27-pip git wget
  sudo /usr/bin/pip install ansible
  sudo git clone https://krastanov@bitbucket.org/krastanov/ansible.git /playbooks
  sudo /usr/local/bin/ansible-playbook /playbooks/nmon.yml
  EOF
}
